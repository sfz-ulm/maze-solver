#!/bin/python3
import pygame

# configs
playerChar, wallChar, finishChar = 'p', '#', 'f'
mapFile = 'map.txt'
stepTime = 100  # in seconds
tileSize = 42

# positions
mapWidth, mapHeight = 0, 0
playerX, playerY = 0, 0
finishX, finishY = 0, 0

# maps
walls = None
trail = None
visited = None


# images
def loadTile(filename):
    return pygame.transform.scale(
        pygame.image.load(filename),
        (tileSize, tileSize))


floorTile = loadTile('img/floor.png')
wallTile = loadTile('img/wall.png')
playerTile = loadTile('img/player.png')
trailTile = loadTile('img/trail.png')
finishTile = loadTile('img/finish.png')
visitedTile = loadTile('img/visited.png')


def processEvents():
    for event in pygame.event.get():
        if event.type == pygame.QUIT: exit()


def emptyMap():
    return [[False for _ in range(mapHeight)] for _ in range(mapWidth)]


def loadMap():
    global walls,  mapWidth, mapHeight, playerX, playerY, finishX, finishY, mapFile
    mapFile = open(mapFile).read().splitlines()
    # first line is map size, rest is the actual map
    (mapWidth, mapHeight), mapText = eval(mapFile[0]), mapFile[1:]

    # fill walls and set player, finish
    walls = emptyMap()
    for x in range(0, mapWidth):
        for y in range(0, mapHeight):
            if y >= len(mapText): continue
            if x >= len(mapText[y]): continue
            if mapText[y][x] == playerChar:
                playerX, playerY = x, y
            if mapText[y][x] == finishChar:
                finishX, finishY = x, y
            if mapText[y][x] == wallChar:
                walls[x][y] = True

def drawTile(tile, x, y): screen.blit(tile, (x * tileSize, y * tileSize))


def render():
    for x in range(mapWidth):
        for y in range(mapHeight):
            # wall or floor
            if walls[x][y]: drawTile(wallTile, x, y)
            else:           drawTile(floorTile, x, y)
            # trail
            if trail[x][y]:     drawTile(trailTile, x, y)
            elif visited[x][y]: drawTile(visitedTile, x, y)
    drawTile(finishTile, finishX, finishY)
    drawTile(playerTile, playerX, playerY)

    pygame.display.flip()


# init maps
loadMap()
trail = emptyMap()
visited = emptyMap()

# init pygame
pygame.init()
pygame.display.set_caption('maze solver')
screen = pygame.display.set_mode((tileSize * mapWidth, tileSize * mapHeight))

# your code here
def step(x, y):
    global trail, visited, playerX, playerY
    # check for map boundaries
    if x < 0 or y < 0 or x >= mapWidth or y >= mapHeight:
        return False
    # check for obstacles
    if walls[x][y]:
        return False
    # check if been here previously
    if visited[x][y]:
        return False

    pygame.time.delay(stepTime)

    playerX, playerY = x, y
    trail[x][y] = visited[x][y] = True

    render()
    if (x, y) == (finishX, finishY):
        return True

    if step(x, y - 1):
        return True
    if step(x + 1, y):
        return True
    if step(x, y + 1):
        return True
    if step(x - 1, y):
        return True

    trail[x][y] = False
    return False

step(playerX, playerY)

# wait for window close when finished
while True:
    processEvents()
    pygame.time.delay(100)
