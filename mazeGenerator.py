#!/bin/python3
import pygame
import random

# configs
playerChar, wallChar, finishChar = 'p', '#', 'f'
mapFile = 'map.txt'
stepTime = 100  # in seconds
tileSize = 16

# positions
mapWidth, mapHeight = 49,49
playerX, playerY = 0, 0
finishX, finishY = 0, 0

# maps
walls = None
trail = None
visited = None


# images
def loadTile(filename):
    return pygame.transform.scale(
        pygame.image.load(filename),
        (tileSize, tileSize))


floorTile = loadTile('img/floor.png')
wallTile = loadTile('img/wall.png')
playerTile = loadTile('img/player.png')
trailTile = loadTile('img/trail.png')
finishTile = loadTile('img/finish.png')
visitedTile = loadTile('img/visited.png')


def processEvents():
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            exit()


def emptyMap():
    return [[False for _ in range(mapHeight)] for _ in range(mapWidth)]


def fullMap():
    return [[True for _ in range(mapHeight)] for _ in range(mapWidth)]


def loadMap():
    global walls,  mapWidth, mapHeight, playerX, playerY, finishX, finishY, mapFile
    mapFile = open(mapFile).read().splitlines()
    # first line is map size, rest is the actual map
    (mapWidth, mapHeight), mapText = eval(mapFile[0]), mapFile[1:]

    # fill walls and set player, finish
    walls = emptyMap()
    for x in range(0, mapWidth):
        for y in range(0, mapHeight):
            if y >= len(mapText):
                continue
            if x >= len(mapText[y]):
                continue
            if mapText[y][x] == playerChar:
                playerX, playerY = x, y
            if mapText[y][x] == finishChar:
                finishX, finishY = x, y
            if mapText[y][x] == wallChar:
                walls[x][y] = True


def randomMap(x, y):
    global trail, visited, playerX, playerY

    pygame.time.delay(stepTime)

    playerX, playerY = x, y
    visited[x][y] = True

    render()

    randomOrder = []
    if x+2 < mapWidth and not visited[x + 2][y]:
        randomOrder.append(0)
    if y+2 < mapHeight and not visited[x][y + 2]:
        randomOrder.append(1)
    if x-2 >= 0 and not visited[x - 2][y]:
        randomOrder.append(2)
    if y-2 >= 0 and not visited[x][y - 2]:
        randomOrder.append(3)

    random.shuffle(randomOrder)

    for i in randomOrder:
        if i == 0 and not visited[x+2][y]:
            walls[x + 1][y] = False
            visited[x + 1][y] = True
            randomMap(x + 2, y)
        if i == 1 and not visited[x][y+2]:
            walls[x][y + 1] = False
            visited[x][y + 1] = True
            randomMap(x, y + 2)
        if i == 2 and not visited[x-2][y]:
            walls[x - 1][y] = False
            visited[x - 1][y] = True
            randomMap(x - 2, y)
        if i == 3 and not visited[x][y-2]:
            walls[x][y - 1] = False
            visited[x][y - 1] = True
            randomMap(x, y - 2)

    return False

def generateRandomMap():
    global walls, visited, trail, playerX, playerY

    visited = emptyMap()
    walls = fullMap()
    trail = emptyMap()

    for i in range(0, mapWidth, 2):
        for j in range(0, mapHeight, 2):
            walls[i][j] = False
    playerX, playerY = random.randint(0, round(mapWidth/2))*2, random.randint(0, round(mapHeight/2))*2
    randomMap(playerX, playerY)

def drawTile(tile, x, y): screen.blit(tile, (x * tileSize, y * tileSize))


def render():
    for x in range(mapWidth):
        for y in range(mapHeight):
            # wall or floor
            if walls[x][y]:
                drawTile(wallTile, x, y)
            else:
                drawTile(floorTile, x, y)
            # trail
            if trail[x][y]:
                drawTile(trailTile, x, y)
            elif visited[x][y]:
                drawTile(visitedTile, x, y)
    drawTile(finishTile, finishX, finishY)
    drawTile(playerTile, playerX, playerY)

    pygame.display.flip()


# init pygame
pygame.init()
pygame.display.set_caption('maze solver')
screen = pygame.display.set_mode((tileSize * mapWidth, tileSize * mapHeight))

# your code here
# init maps
#loadMap()
generateRandomMap()
trail = emptyMap()
visited = emptyMap()


def step(x, y) -> bool:
    global trail, visited, playerX, playerY

    # check stuff

    pygame.time.delay(stepTime)

    playerX, playerY = x, y
    trail[x][y] = visited[x][y] = True

    render()

    if (x, y) == (finishX, finishY):
        return True

    # move
    if x+1 < mapWidth and not walls[x+1][y] and not visited[x+1][y]:
        if step(x+1, y):
            return True
    if y+1 < mapHeight and not walls[x][y+1] and not visited[x][y+1]:
        if step(x, y+1):
            return True
    if x-1 >= 0 and not walls[x-1][y] and not visited[x-1][y]:
        b = step(x-1, y)
        if b:
            return True
    if y-1 >= 0 and not walls[x][y-1] and not visited[x][y-1]:
        if step(x, y-1):
            return True

    trail[x][y] = False
    return False


step(playerX, playerY)

# wait for window close when finished
while True:
    processEvents()
    pygame.time.delay(100)
